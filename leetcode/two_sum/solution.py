#!/usr/bin/env python3
from typing import List

def twoSum(nums: List[int], target: int) -> List[int]:
    # 
    for i in range(0, len(nums)):
        n = nums[i]
        for j in range(i + 1, len(nums)):
            if n + nums[j] == target:
                return [i, j]


# Check 1
nums = [2, 7, 11, 15]
target = 9
desired_output = [0, 1]
output = twoSum(nums, target)
print(f"output = {output}")
print(f"Matches output? {output == desired_output}")

# Check 2
nums = [3, 2, 4]
target = 6
desired_output = [1, 2]
output = twoSum(nums, target)
print(f"output = {output}")
print(f"Matches output? {output == desired_output}")
